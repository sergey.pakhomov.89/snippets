#!/bin/bash

function drop_rules()
{
  # remove all rules
  while IFS= read -r line
  do
    zmctl net del $(echo $line | cut -d " " -f1)
  done < <(zmctl net) 
  
  while IFS= read -r line
  do
    zmctl zone del $(echo $line | cut -d " " -f1)
  done < <(zmctl zone) 
}

function reset_rules()
{
  drop_rules

  cipso="s3:c9"

  zmctl net add 10.248.192/26 doi:1 mls:$cipso
  zmctl net add 10.121.21/24 doi:1 mls:$cipso unaware
  zmctl net add 10.1/16 doi:1 mls:$cipso unaware
  
  zmctl zone add one-0-0 doi:1 mls:$cipso unaware
}

function usage()
{
	echo "$0 [drop|reset|status]"
	exit 1
}

function show_status()
{
  echo "=== > zmctl status:"
  zmctl net; zmctl zone
}


case "$1" in
drop)
	drop_rules
	show_status
	;;
reset)
	reset_rules
	show_status
	;;
status)
	show_status
	;;
*)
	usage
	;;
esac
