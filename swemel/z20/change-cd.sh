#!/bin/bash

wd='/var/spool/swemel'

[[ "$wd" != $(pwd) ]] && { echo "Error: run from $wd"; exit 1; }

[[ -z "$1" ]] && { echo "Error: specify iso image!"; exit 1; }

echo -n "Remount... "
umount z20 &>/dev/null
ln -sf "$1" ./z20.latest.iso  &>/dev/null
mount -o loop "$1" ./z20 &>/dev/null
echo "done"

echo "Check:"
mount | grep z20
ls -l | grep 'z20.latest.iso'
